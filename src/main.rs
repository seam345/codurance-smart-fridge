type Date<'a> = &'a str;
type ItemName<'a> = &'a str;
type ExpiryDate<'a> = &'a str;
type ItemCondition<'a> = &'a str;
struct Fridge{}

fn main() {
    println!("Hello, world!");
}

fn set_current_date<T>(_fridge: T, _date: Date) -> T {
todo!()
}
fn signal_fridge_door_opened<T>(_fridge: T) -> T {
todo!()
}
fn scan_added_item<T>(_fridge: T, _name: ItemName, _expiry: ExpiryDate, _condition: ItemCondition ) -> T {
todo!()
}
fn signal_fridge_door_closed<T>(_fridge: T) -> T {
todo!()
}
fn simulate_day_over<T>(_fridge: T) -> T {
todo!()
}
fn scan_removed_item<T>(_fridge:T, _name: ItemName) -> T {
todo!()
}
fn show_display<T>(_fridge: &T) -> String{
    todo!()
}

#[cfg(test)]
mod tests {
    use crate::{Fridge, simulate_day_over, set_current_date, signal_fridge_door_opened, scan_added_item, signal_fridge_door_closed, scan_removed_item, show_display};
    #[test]
    fn full_test() {
        let fridge = Fridge{};
        // Setup:
        let fridge = set_current_date(fridge, "18/10/2021");
        // Input:
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = scan_added_item(fridge, "Milk",  "21/10/21", "sealed");
        let fridge = scan_added_item( fridge, "Cheese",  "18/11/21",  "sealed");
        let fridge = scan_added_item( fridge, "Beef",  "20/10/21",  "sealed");
        let fridge = scan_added_item( fridge, "Lettuce",  "22/10/21",  "sealed");
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = simulate_day_over(fridge);
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = scan_removed_item( fridge, "Milk");
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = scan_added_item( fridge, "Milk",  "26/10/21",  "opened");
        let fridge = scan_added_item( fridge, "Peppers",  "23/10/21",  "opened");
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = simulate_day_over(fridge);
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = scan_removed_item( fridge, "Beef");
        let fridge = scan_removed_item( fridge, "Lettuce");
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = scan_added_item( fridge, "Lettuce",  "22/10/21",  "opened");
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = signal_fridge_door_opened(fridge);
        let fridge = signal_fridge_door_closed(fridge);
        let fridge = simulate_day_over(fridge);
        // Output:
        show_display(&fridge);
    }
}